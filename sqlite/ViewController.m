//
//  ViewController.m
//  sqlite
//
//  Created by click labs 115 on 11/24/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    NSMutableArray *days;
    NSMutableArray *dates;
    NSMutableArray *jsonList;
    NSMutableDictionary *strShowDataInDestination;

}
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) IBOutlet UIButton *btnSave;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString *URL = [NSString
                     stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast/daily?q=London&mode=json&units=metric&cnt=7&appid=2de143494c0b295cca9337e1e96b00e0"];
    NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:URL]];
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    jsonList = json[@"list"];
    days = [NSMutableArray new];
    dates = [NSMutableArray new];
    
    for (int i =0;i<jsonList.count;i++) {
        
        NSDictionary *weather = [jsonList objectAtIndex: i];
        NSDictionary *temp = weather[@"temp"];
        NSNumber *min = temp[@"min"];
        NSNumber *max = temp[@"max"];
        NSString *dt = weather[@"dt"];
        
        
        NSString * timeStampString =[NSString stringWithFormat:@"%@ ",dt];
        NSTimeInterval _interval=[timeStampString doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
        NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
        [_formatter setDateFormat:@"dd/MM/yy"];
        [_formatter stringFromDate:date];
        
        [days addObject:[NSString stringWithFormat:@"%@°C/%@°C",max,min]];
        
        for (int j = 0; j<1; j++) {
            [dates addObject:[NSString stringWithFormat:@"%@", [_formatter stringFromDate:date]]];
        }
        
        
        
        
        NSLog(@"%@",days);
        
        
        
    }
    
    [_tblView reloadData];
}
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return days.count;
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse"];
    UILabel *lblDate =(UILabel*) [cell viewWithTag:1];
    lblDate.text =  dates [indexPath.row] ;
    ([cell.contentView addSubview:lblDate]);
    
    
    UILabel *lblTemp =(UILabel*) [cell viewWithTag:2];
    lblTemp.text =  days [indexPath.row] ;
    ([cell.contentView addSubview:lblTemp]);
    return  cell;
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
